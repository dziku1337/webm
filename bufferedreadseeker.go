package webm

import (
	"fmt"
	"io"
)

type BufferedReadSeeker struct {
	reader    io.Reader
	buffer    []byte
	offset    int64
	totalRead int64
	grow      bool
	fixedSize int64
}

func NewBufferedReadSeeker(r io.Reader) *BufferedReadSeeker {
	return &BufferedReadSeeker{reader: r, buffer: make([]byte, 0), offset: 0, totalRead: 0, grow: true}
}

func min(a, b int64) int64 {
	if a < b {
		return a
	}
	return b
}

func max(a, b int64) int64 {
	if a > b {
		return a
	}
	return b
}

func (b *BufferedReadSeeker) Read(p []byte) (n int, err error) {
	if b.offset < b.totalRead-int64(len(b.buffer)) {
		err = fmt.Errorf("Cannot read from the forgotten part. B.offset: %d, B.totalRead: %d, Buffer length: %d, B.grow %v", b.offset, b.totalRead, len(b.buffer), b.grow)
		return
	}

	toRead := int64(len(p))
	bufLen := int64(len(b.buffer))
	nCopiedFromBuffer := min(toRead, min(bufLen, b.totalRead-b.offset))
	if nCopiedFromBuffer > 0 {
		bufBegin := b.totalRead - bufLen
		copy(p, b.buffer[b.offset-bufBegin:])
		b.offset += nCopiedFromBuffer
	}

	if nCopiedFromBuffer == toRead {
		return int(nCopiedFromBuffer), nil
	}

	n, err = b.reader.Read(p[nCopiedFromBuffer:])
	if err != nil {
		return int(nCopiedFromBuffer) + n, err
	}

	readBytes := int64(n)
	b.offset += readBytes
	b.totalRead += readBytes
	readBytes += nCopiedFromBuffer
	if b.grow {
		b.buffer = append(b.buffer, p[nCopiedFromBuffer:readBytes]...)
	} else {
		// Keep last b.fixedSize bytes
		currBufferSuffix := b.buffer[int64(len(b.buffer))-max(0, b.fixedSize-readBytes):]
		copy(b.buffer, append(currBufferSuffix, p[max(0, readBytes-b.fixedSize):readBytes]...))
	}

	return int(readBytes), nil
}

func (b *BufferedReadSeeker) Seek(offset int64, whence int) (ret int64, err error) {
	if whence == 2 {
		err = fmt.Errorf("Cannot seek from the end. Offset %d, whence %d", offset, whence)
		return
	} else if whence < 0 || whence > 2 {
		err = fmt.Errorf("Unknown whence. Offset %d, whence %d", offset, whence)
		return
	}

	var target int64
	if whence == 0 {
		target = offset
	} else {
		target = b.offset + offset
	}

	if target < 0 {
		err = fmt.Errorf("Cannot seek past the beginning of the file. Offset %d, whence %d", offset, whence)
		return
	} else if target <= b.totalRead {
		b.offset = target
	} else {
		toRead := target - b.totalRead
		b.offset = b.totalRead
		p := make([]byte, toRead)
		_, err := b.Read(p)
		if err != nil {
			return b.offset, err
		}
	}

	return b.offset, nil
}

func (b *BufferedReadSeeker) FixSize(fixedSize int64) {
	b.grow = false
	b.fixedSize = fixedSize
	b.buffer = b.buffer[int64(len(b.buffer))-b.fixedSize:]
}
